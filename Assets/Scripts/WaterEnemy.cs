
using UnityEngine;

// INHERITANCE
class WaterEnemy : Enemy {
    [SerializeField] private float radius = 5.0F;
    [SerializeField] private float power = 10.0F;

    // POLYMORPHISM
    protected override void OnCollisionWithSphere(Collision other) {
        Rigidbody rigidbody;

        foreach (Collider hit in Physics.OverlapSphere(transform.position, radius)) {
            rigidbody = hit.GetComponent<Rigidbody>();

            if (rigidbody != null)
                rigidbody.AddForce(Vector3.up * power, ForceMode.Impulse);
        }
    }

    protected override void OnPlayCollisionParticle() {
        Instantiate(collisionParticle.gameObject, transform.position, collisionParticle.gameObject.transform.rotation);
    }
}
