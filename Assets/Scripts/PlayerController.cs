
using System.Collections;
using UnityEngine;

// INHERITANCE
public class PlayerController : SphereController {
    [SerializeField] private float movementSpeed;
    [SerializeField] private float turnSpeed;
    [SerializeField] private float powerForce;
    private bool isUsingPower;
    private CameraController cameraController;
    [SerializeField] private ParticleSystem flameStream;
    [SerializeField] private AudioClip flameStreamAudio;

    protected override void Start() {
        base.Start();
        
        cameraController = GameObject.Find("FocalPoint").GetComponent<CameraController>();
    }

    void FixedUpdate() {
        if (Input.GetKey(KeyCode.Space)) {
            if (!isUsingPower) {
                isUsingPower = true;
                flameStream.Play();
                audioSource.PlayOneShot(flameStreamAudio, 1f);
            }

            sphereRigidbody.AddForce(cameraController.direction * powerForce);
        } else {
            if (isUsingPower) {
                isUsingPower = false;
                flameStream.Stop();
            }

            sphereRigidbody.AddForce(cameraController.direction * Input.GetAxis("Vertical") * movementSpeed);
            sphereRigidbody.AddTorque(Vector3.up * Input.GetAxis("Horizontal") * turnSpeed);
        }
    }

    // POLYMORPHISM
    protected override void OnCollisionWithSphere(Collision other) {
        GameManager.Instance.SendNewMagnitude(other.relativeVelocity.magnitude);
    }
}
