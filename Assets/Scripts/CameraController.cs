
using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField] private GameObject target;
    [SerializeField] private GameObject gameOverPoint;
    [SerializeField] private float rotationSpeed = 200f;

    // ENCAPSULATION
    public Vector3 direction { get; private set; }

    void LateUpdate() {
        if (target == null) {
            target = GameManager.Instance.isGameActive ?
                GameManager.Instance.Player : gameOverPoint;
            
            transform.rotation = target.transform.rotation;
        }
        
        transform.position = target.transform.position;

        if (GameManager.Instance.isGameActive) {
            if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1)) {
                transform.Rotate(Vector3.left, Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime);
                transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime, Space.World);
            }
            
            direction = Input.GetKey(KeyCode.Mouse0) ? transform.right * -1 : transform.forward;
        }
    }
}
