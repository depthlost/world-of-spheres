
using UnityEngine;

// INHERITANCE
public class Enemy : SphereController {    
    void Update() {
        if (transform.position.y < -2)
            Destroy(gameObject);
    }

    // POLYMORPHISM
    protected override void OnCollisionWithSphere(Collision other) {
        sphereRigidbody.AddForce((transform.position - other.gameObject.transform.position) * 10f, ForceMode.Impulse);
    }
}
