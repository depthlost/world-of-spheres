
using UnityEngine;

public class SphereController : MonoBehaviour {
    [SerializeField] protected ParticleSystem collisionParticle;
    protected AudioSource audioSource;
    [SerializeField] private AudioClip collisionAudio;

    protected Rigidbody sphereRigidbody;

    protected virtual void Start() {
        sphereRigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    protected void OnCollisionEnter(Collision other) {
        if (other.gameObject.CompareTag("Sphere")) {
            // Debug.Log(gameObject.name + ", relativeVelocity: " + other.relativeVelocity + ", magnitude: " + other.relativeVelocity.magnitude);
            
            OnCollisionWithSphere(other);
            OnPlayCollisionParticle();
            audioSource.PlayOneShot(collisionAudio, 1f);
        }
    }

    protected virtual void OnCollisionWithSphere(Collision other) {

    }

    protected virtual void OnPlayCollisionParticle() {
        collisionParticle.Play();
    }
}
