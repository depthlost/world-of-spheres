using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    // ENCAPSULATION
    public bool isGameActive { get; private set; } = true;
    private int round = 0;
    private int lifes = 3;
    [SerializeField] private float maximumMagnitude;
    [SerializeField] private GameObject playerPrefab;
    private SpawnManager spawnManager;
    [SerializeField] private TextMeshProUGUI newRoundText;
    [SerializeField] private TextMeshProUGUI roundText;
    [SerializeField] private TextMeshProUGUI lifesText;
    [SerializeField] private TextMeshProUGUI gameOverText;
    [SerializeField] private TextMeshProUGUI maximumMagnitudeText;
    [SerializeField] private Button restartButton;
    // ENCAPSULATION
    public GameObject Player { get; private set; }

    public static GameManager Instance;

    void Awake() {
        Instance = this;        
        SpawnPlayer();
    }

    void Start() {
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

        restartButton.onClick.AddListener(RestartGame);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        UpdateLifes(lifes);
        NextRound();
    }

    void Update() {
        if (isGameActive && Player.transform.position.y < -2)
            LosePlayer();
    }

    // ABSTRACTION
    private void UpdateLifes(int lifes) {
        this.lifes = lifes;
        lifesText.text = "Lifes: " + this.lifes;
    }

    // ABSTRACTION
    private void LosePlayer() {
        UpdateLifes(lifes - 1);

        if (lifes > 0)
            RespawnPlayer();
        else
            GameOver();
    }

    // ABSTRACTION
    private void SpawnPlayer() {
        Player = Instantiate(playerPrefab, new Vector3(0, 1, 0), playerPrefab.transform.rotation);
    }

    // ABSTRACTION
    private void RespawnPlayer() {
        Destroy(Player);
        SpawnPlayer();
    }

    private void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void GameOver() {
        isGameActive = false;
        Destroy(Player);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        lifesText.gameObject.SetActive(false);

        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);

        roundText.text = "Round: " + round;
        roundText.gameObject.SetActive(true);

        UpdateMagnitudeText();
        maximumMagnitudeText.gameObject.SetActive(true);
    }

    public void NextRound() {
        round += 1;
        spawnManager.StartRound(round);
        
        if (round % 5 == 0)
            UpdateLifes(lifes + 1);

        StartCoroutine(ShowNewRoundText());
    }

    private IEnumerator ShowNewRoundText() {
        newRoundText.text = "Round " + round;
        newRoundText.gameObject.SetActive(true);
        
        yield return new WaitForSeconds(3);
        
        newRoundText.gameObject.SetActive(false);
    }

    public void SendNewMagnitude(float magnitude) {
        maximumMagnitude = Mathf.Max(magnitude, maximumMagnitude);
    }

    private void UpdateMagnitudeText() {
        maximumMagnitudeText.text = "Magnitude: " + maximumMagnitude.ToString("0.00");
    }
}
