
using UnityEngine;

// INHERITANCE
class ExplosiveEnemy : Enemy {
    [SerializeField] private float radius = 5.0F;
    [SerializeField] private float power = 10.0F;

    // POLYMORPHISM
    protected override void OnCollisionWithSphere(Collision other) {
        Vector3 explosionPosition = transform.position;
        Rigidbody rigidbody;

        foreach (Collider hit in Physics.OverlapSphere(explosionPosition, radius)) {
            rigidbody = hit.GetComponent<Rigidbody>();

            if (rigidbody != null)
                rigidbody.AddExplosionForce(power, explosionPosition, radius, 3f);
        }
    }

    protected override void OnPlayCollisionParticle() {
        Instantiate(collisionParticle.gameObject, transform.position, transform.rotation);
    }
}
