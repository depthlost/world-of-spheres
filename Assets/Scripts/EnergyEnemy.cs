
using System.Collections;
using UnityEngine;

// INHERITANCE
class EnergyEnemy : Enemy {
    [SerializeField] private float radius = 20.0F;
    [SerializeField] private float power = 1000.0F;
    private bool hasPreviousCollision = false;
    [SerializeField] private GameObject explosionPower;
    [SerializeField] private ParticleSystem explosionParticle;

    // POLYMORPHISM
    protected override void OnCollisionWithSphere(Collision other) {
        if (!hasPreviousCollision && other.relativeVelocity.magnitude < 60) {
            Vector3 explosionPosition = transform.position;
            Rigidbody rigidbody;

            foreach (Collider hit in Physics.OverlapSphere(explosionPosition, radius)) {
                rigidbody = hit.GetComponent<Rigidbody>();

                if (rigidbody != null)
                    rigidbody.AddExplosionForce(power, explosionPosition, radius);
            }

            StartCoroutine(ExplodeCoroutine());
        } else {
            Instantiate(explosionPower, transform.position, explosionPower.transform.rotation);
            hasPreviousCollision = false;
        }
    }

    private IEnumerator ExplodeCoroutine() {
        hasPreviousCollision = true;
        yield return new WaitForSeconds(2);
        
        if (hasPreviousCollision) {
            explosionParticle.Play();
            hasPreviousCollision = false;
        }
    }
}
