using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPower : MonoBehaviour
{
    [SerializeField] private float radius = 200.0F;
    [SerializeField] private float power = 1000.0F;
    [SerializeField] private float upwardsModifier = 200.0F;

    [SerializeField] private AudioClip explosionAudio;

    void Start()
    {
        Rigidbody rigidbody;

        foreach (Collider hit in Physics.OverlapSphere(transform.position, radius)) {
            // if (true || hit.gameObject.GetComponent<PlayerController>() == null) {}
            rigidbody = hit.GetComponent<Rigidbody>();

            if (rigidbody != null)
                rigidbody.AddExplosionForce(power, transform.position, radius, upwardsModifier);
        }

        GetComponent<AudioSource>().PlayOneShot(explosionAudio, 1f);
    }
}
