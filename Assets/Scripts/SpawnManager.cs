using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> enemyPrefabs;
    [SerializeField] private float xRange;
    [SerializeField] private float zRange;
    [SerializeField] private float yPosition;
    [SerializeField] private float initialSpawnTime;
    [SerializeField] private float spawnTime;
    [SerializeField] private int numberOfEnemiesRemaining;
    [SerializeField] private int maximumEnemies;
    
    public void StartRound(int round) {
        StartCoroutine(Spawn(round));
    }

    private IEnumerator Spawn(int round) {
        spawnTime = 8f * Mathf.Pow(0.8f, round - 1);
        numberOfEnemiesRemaining = round * 3 + Mathf.FloorToInt(round / 10) * 30;

        yield return new WaitForSeconds(initialSpawnTime);

        while (GameManager.Instance.isGameActive && numberOfEnemiesRemaining > 0) {
            if (GetNumberOfActiveEnemies() < maximumEnemies) {
                SpawnEnemy();
                numberOfEnemiesRemaining -= 1;
            }
            
            yield return new WaitForSeconds(spawnTime);
        }

        while (GameManager.Instance.isGameActive && GetNumberOfActiveEnemies() > 0)
            yield return new WaitForSeconds(2f);

        if (GameManager.Instance.isGameActive)
            GameManager.Instance.NextRound();
    }

    private void SpawnEnemy() {
        GameObject enemyPrefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];

        Instantiate(
            enemyPrefab,
            new Vector3(
                Random.Range(-xRange, xRange),
                yPosition,
                Random.Range(-zRange, zRange)
            ),
            enemyPrefab.transform.rotation
        );
    }

    private int GetNumberOfActiveEnemies() => FindObjectsOfType<Enemy>().Length;
}
