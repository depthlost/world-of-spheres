using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button startButton;

    void Start() {
        startButton.onClick.AddListener(StartGame);
    }

    private void StartGame() {
        SceneManager.LoadScene("Game");
    }
}
