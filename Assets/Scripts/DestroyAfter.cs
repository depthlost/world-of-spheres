
using UnityEngine;

public class DestroyAfter : MonoBehaviour {
    [SerializeField] private float seconds;
    
    void Start() {
        Destroy(gameObject, seconds);
    }
}
